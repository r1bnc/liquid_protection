
--[[
Restrict liquid placement
Modification Copyright (C) 2022  R1BNC
Original program by http://github.com/SmallJoker (c) SmallJoker

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]--


minetest.register_privilege( "liquids", "Can place liquid at any depth.")

local LIQUID_PLACE_LEVEL = 0

function override_on_place(item_name)
	local def = minetest.registered_items[item_name]
	local old_on_place = def.on_place

	def.on_place = function(itemstack, placer, pointed_thing)
		local player_name = placer:get_player_name()
		if pointed_thing.type ~= "node" then
			return itemstack -- No placement
		end
		if pointed_thing.above.y <= LIQUID_PLACE_LEVEL
				or minetest.check_player_privs(placer, "liquids") then
			-- OK
			minetest.log("action", "[liquidprotection] "..player_name .. " placed liquid "..item_name.." on an area.")
			return old_on_place(itemstack, placer, pointed_thing)
		end
		-- Prevent placement
		minetest.chat_send_player(player_name,
			"You are not allowed to place liquids above " .. LIQUID_PLACE_LEVEL .. "!")
		minetest.log("action", "[liquidprotection] "..player_name .. " tried to place "..item_name.." liquid above " .. LIQUID_PLACE_LEVEL)
		return itemstack
	end
end

override_on_place("bucket:bucket_lava")
override_on_place("bucket:bucket_water")
override_on_place("bucket:bucket_river_water")
override_on_place("default:river_water_source")
override_on_place("default:water_source")
override_on_place("default:lava_source")