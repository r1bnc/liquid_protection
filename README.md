# Protection from spilled liquids

Modified by R1BNC 2022

This mod protects server from griefing by spilling luquids like lava and water above curtain level. Initial code is not mine, found it on the forum and changed a bit to protect not only from lava, but also from water. Other liquids can be added as well. Initial code author is SmallJoker (http://github.com/SmallJoker).
